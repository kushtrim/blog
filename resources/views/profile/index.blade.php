@extends('layouts.app')

@section('content')
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Profile</title>
</head>
<body>

	<h1>This is your Profile</h1>
	
	<table class="table table-striped table-responsive">
		<thead>
			<tr>
				<th>ID</th>
				<th>Name</th>
				<th>Email</th>
			</tr>
		</thead>
		<tbody>
			@foreach($users as $user)
				<tr>
					<!-- te dhenat -->
					<td>{{$user->id}}</td>
					<td>{{$user->name}}</td>
					<td>{{$user->email}}</td>
				</tr>
			@endforeach
		</tbody>
	</table>
</body>
</html>
@endsection
